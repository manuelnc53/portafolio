/***********************
  Menu Component
 ***********************/
import React from 'react';
import SocialLinks from './SocialLinks'
const Menu = props => {
    return (
      <div className={`menu-container ${props.showMenu}`}>
        <div className="overlay" />
        <div className="menu-items">
          <ul>
            <li>
              <a href="#welcome-section" onClick={props.toggleMenu}>
                HOME
              </a>
            </li>
            <li>
              <a href="#about" onClick={props.toggleMenu}>
                SOBRE MÍ
              </a>
            </li>
            <li>
              <a href="#projects" onClick={props.toggleMenu}>
                PORTAFOLIO
              </a>
            </li>
            <li>
              <a href="#contact" onClick={props.toggleMenu}>
                CONTACTO    
              </a>
            </li>
          </ul>
          <SocialLinks />
        </div>
      </div>
    );
  };
  export default Menu  