/***********************
  About Component
 ***********************/
import React from 'react';
const About = props => {
    return (
      <section id="about">
        <div className="wrapper">
          <article>
            <div className="title">
              <h3>Sobre mí</h3>
              <p className="separator" />
            </div>
            <div className="desc full">
              <h4 className="subtitle">Mi nombre es Manuel Nuñez.</h4>
              <p>
                Soy un desarrollador de software y emprendedor de la ciudad de San Luis, Argentina.
              </p>
              <p>
                Disfruto mucho encontrarme con nuevos desafíos en el area del desarrollo de software.  
                Adopto una filosofía de constante aprendizaje, ya que en una industria de constante cambio 
                como lo es la industria del software es importante estar siempre pendiente de las novedades
                tecnológicas.

              </p>
            </div>
            <div className="title">
              <h3>¿Qué es lo que hago?</h3>
              <p className="separator" />
            </div>
            <div className="desc">
              <h4 className="subtitle">Desarrollo de software.</h4>
              <p>
                Tengo un perfil de desarrollador web frontend, mis tecnologías favoritas a la hora de 
                desarrollar son: CSS, HTML, JavaScript, también utilizo frameworks como Angular y ReactJS para agilizar mis 
                proyectos y hacerlos escalables. 
              </p>
              <p>
                Para realizar aplicaciones web también me capacité en tecnologías backend, como lo son
                API REST, NodeJS, Sequalize, MySQL, y Java. En algunos proyectos para trabajar la parte 
                de Backend también utilizo el servicio de Firebase que proporciona Google.               
              </p>
              <p>
                El conocimiento teorico adquirido durante mi transcurso como estudiante de la carrega de
                ingeniería en informatica de la Universidad Naciona de San Luis, me ha permitido tener el
                conocimiento teórico práctico en; sistemas operativos, redes de computadoras, estructuras de datos,
                fundamentos de la computación, arquitectura de software, bases de datos, ingeniería del software.
                Estos conocimientos me dan las herramientas para producir, mantener y mejorar software de calidad industrial.
              </p>
            </div>
            <div className="desc">
              <h4 className="subtitle">Emprendimiento.</h4>
              <p>
                Como desarrollador de software en la Argentina me he dado cuenta de la necesidad que hay
                de innovar con la Tecnología Informatica en distintos sectores del país, para así brindar 
                a las empresas herramientas para impulsarse economicamente. Esto me ha motivado a 
                involucrarme de manera activa en el emprendimiento para brindarle soluciones tecnológicas
                a las empresas de Argentina. 
              </p>
              <p>
                La política es un medio para poder brindarle a las empresas más oportunidades de poder
                acceder a servicios informáticos que potencien sus emprendimientos. En enero de 2020 me 
                uní a la Cámara de Comercio de San Luis para poder colaborar en el crecimiento de los 
                emprendimientos tecnológicos de la provincia.
              </p>
            </div>
          </article>
        </div>
      </section>
    );
  };

  export default About