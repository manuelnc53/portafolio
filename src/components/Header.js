/***********************
  Header Component
 ***********************/
import React from 'react';
import imagen from '../css/bg.png'; 
const Header = props => {
    return (
      <header id="welcome-section">
        <div className="forest" />
        
        <div className="container">
        <div className="row">
            <div className="col-md-6">
                <img className="imagen" src={imagen}></img>
            </div>
            <div className="col-md-6">
            <h1>
            
            <span className="line">Desarrollador web</span>
            <span className="line">
              <span className="color">&</span> emprendedor.
            </span>
          </h1>
          <div className="buttons">
            <a href="#projects">Mi portafolio</a>
            <a href="#contact" className="cta">
              Contacto
            </a>
          </div>
            </div>
        </div>
          
        </div>
      </header>
    );
  };

  export default Header